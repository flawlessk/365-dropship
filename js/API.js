import { WEB_ADDRES } from "./cfg.js";


const call = async (url) => {
    const request = await fetch(WEB_ADDRES + url);
    const result = await request.json();
    return result;
}
export const getProducts = async () => await call("/products");
export const getCategories = async () => await call("/products/categories");
