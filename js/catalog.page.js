import { getProducts, getCategories } from "./API.js";
import { productHTML, sortedHTML } from "./product.js";


//  ALL PRODUCTS IN CATALOG
const catalog = document.getElementById("products-wrapper");
const productsInCatalog = async () => {
    const productsInfo = await getProducts();
    let information = "";
    for(const info of productsInfo) {
        information +=  productHTML(info);
    } 
    catalog.innerHTML = information;
}
productsInCatalog();
// SORT (Price : High to low & reversed)

const sort = document.getElementById("sort");

const sortFunction = async () => {
    const productsData = await getProducts();
    let sortedData = [];
    if(sort.value == "HTL") {
         sortedData = productsData.sort((a,b) => b.price - a.price);
    } else if(sort.value == "LTH") {
         sortedData = productsData.sort((a,b) => a.price - b.price);
    }
    catalog.innerHTML = sortedHTML(sortedData);
}

sort.addEventListener("change", sortFunction);

// Search
const search = document.getElementById("search");
const searchBtn = document.getElementById("search-btn");





























// if(sort.value == "asc") {
//     listArr = list.sort((a, b) => b.price - a.price;
// } else {
//     listArr = list.sort((a, b) => a.price - b.price;
// }