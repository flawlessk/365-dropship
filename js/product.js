export const productHTML = (eachProduct) =>  `
    <div class="products-item">
        <div class="checkbox-container">
            <input type="checkbox" id="products__item--input">
            <label class="check" for="products__item--input">
        </div>
        <div class="products-image">
            <img src="${eachProduct.image}">
        </div>
        <div class="products-title">
            <h3>${eachProduct.title}</h3>
        </div>
        <div class="products-price">
            ${eachProduct.price}$
        </div>
    </div>
`;


export const sortedHTML = (sortedData) => {
    let dataString = "";
    for(let i = 0; i < sortedData.length; i++) {
        dataString += `
        <div class="products-item">
            <div class="checkbox-container">
                <input type="checkbox" id="products__item--input">
            </div>
            <div class="products-image">
                <img src="${sortedData[i].image}">
            </div>
            <div class="products-title">
                <h3>${sortedData[i].title}</h3>
            </div>
            <div class="products-price">
                ${sortedData[i].price}$
            </div>
    </div>
`};
    return dataString;
}
